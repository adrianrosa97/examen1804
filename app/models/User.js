const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  codigo: {
    type: Number,
    unique: true,
    required: true
  },
  nombre: String,
  password: { type: String }
})

UserSchema.pre('save', function (next) {
  const user = this
  if (!user.isModified('password')) return next()

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err)

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(err)

      user.password = hash
      next()
    })
  })
})

const User = mongoose.model('User', UserSchema)

module.exports = User
