const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiasSchema = new Schema({
  codigo: {
    type: String,
    unique: true,
    required: true
  },
  nombre: String,
  curso: String,
  horas: Number
})

const Materia = mongoose.model('Materia', materiasSchema)

module.exports = Materia
