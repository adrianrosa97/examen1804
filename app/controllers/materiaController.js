const Materia = require('../models/Materia')

const index = (req, res) => {
  Materia.find((err, materias) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo las materias'
      })
    }
    return res.json(materias)
  })
}

const show = (req, res) => {
  const id = req.params.id
  Materia.findById(id, function (err, materia) {
    if (err) return res.json({ error: err.errors })
    return res.json(materia)
  })
}

const create = (req, res) => {
  const materia = new Materia(req.body)
  Materia.create(
    {
      codigo: materia.codigo,
      nombre: materia.nombre,
      curso: materia.curso,
      horas: materia.horas
    },
    function (err, materia) {
      if (err) return res.json({ error: err.errors })
      return res.json(materia)
    }
  )
}

const update = (req, res) => {
  const id = req.params.id
  const _materia = new Materia(req.body)

  Materia.findById(id, function (err, materia) {
    if (err) return res.json({ error: err.errors })
    materia.codigo = _materia.codigo
    materia.nombre = _materia.nombre
    materia.curso = _materia.curso
    materia.horas = _materia.horas

    materia.save(function (err) {
      if (err) return res.json({ error: err.errors })
      return res.json(materia)
    })
  })
}
const remove = (req, res) => {
  const id = req.params.id
  Materia.findById(id, function (err, materia) {
    if (err) return res.json({ error: err.errors.name.message })

    materia.remove(function (err) {
      if (err) return res.json({ error: err.errors.name.message })
      return res.json(materia)
    })
  })
}

module.exports = {
  index,
  show,
  create,
  update,
  remove
}
