const User = require('../models/User')
const servicejwt = require('../services/servicejwt')

function login (req, res) {
  const nombre = req.body.nombre
  const password = req.body.password

  User.findOne({ nombre: nombre }).then(function (user) {
    if (user) {
      console.log(user)
      if (user.password == password) {
        return res.status(200).send({
          token: servicejwt.createToken(user),
          codigo: user.codigo,
          nombre: user.nombre
        })
      } else {
        return res.status(404).json({
          message: 'Contraseña incorrecta'
        })
      }
    } else {
      return res.status(404).json({
        message: 'El nombre no existe'
      })
    }
  })
}

module.exports = {
  login
}
